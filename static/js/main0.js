// 'use strict';

// // Set up media stream constant and parameters.

// // In this codelab, you will be streaming video only: "video: true".
// // Audio will not be streamed because it is set to "audio: false" by default.
// const mediaStreamConstraints = {
//   video: true,
// };

// // Set up to exchange only video.
// const offerOptions = {
//   offerToReceiveVideo: 1,
// };

// // Define initial start time of the call (defined as connection between peers).
// let startTime = null;

// // Define peer connections, streams and video elements.
// const localVideo = document.getElementById('localVideo');
// const remoteVideo = document.getElementById('remoteVideo');

// let localStream;
// let remoteStream;

// let localPeerConnection;
// let remotePeerConnection;


// // Define MediaStreams callbacks.

// // Sets the MediaStream as the video element src.
// function gotLocalMediaStream(mediaStream) {
//   localVideo.srcObject = mediaStream;
//   localStream = mediaStream;
//   trace('Received local stream.');
//   callButton.disabled = false;  // Enable call button.
// }

// // Handles error by logging a message to the console.
// function handleLocalMediaStreamError(error) {
//   trace(`navigator.getUserMedia error: ${error.toString()}.`);
// }

// // Handles remote MediaStream success by adding it as the remoteVideo src.
// function gotRemoteMediaStream(event) {
//   const mediaStream = event.stream;
//   remoteVideo.srcObject = mediaStream;
//   remoteStream = mediaStream;
//   trace('Remote peer connection received remote stream.');
// }


// // Add behavior for video streams.

// // Logs a message with the id and size of a video element.
// function logVideoLoaded(event) {
//   const video = event.target;
//   trace(`${video.id} videoWidth: ${video.videoWidth}px, ` +
//         `videoHeight: ${video.videoHeight}px.`);
// }

// // Logs a message with the id and size of a video element.
// // This event is fired when video begins streaming.
// function logResizedVideo(event) {
//   logVideoLoaded(event);

//   if (startTime) {
//     const elapsedTime = window.performance.now() - startTime;
//     startTime = null;
//     trace(`Setup time: ${elapsedTime.toFixed(3)}ms.`);
//   }
// }

// localVideo.addEventListener('loadedmetadata', logVideoLoaded);
// remoteVideo.addEventListener('loadedmetadata', logVideoLoaded);
// remoteVideo.addEventListener('onresize', logResizedVideo);


// // Define RTC peer connection behavior.

// // Connects with new peer candidate.
// function handleConnection(event) {
//   const peerConnection = event.target;
//   const iceCandidate = event.candidate;
//   console.log("iceCandidate:" + iceCandidate);
//   console.log("peerConnection: " + peerConnection);

//   if (iceCandidate) {
//     const newIceCandidate = new RTCIceCandidate(iceCandidate);
//     const otherPeer = getOtherPeer(peerConnection);

//     otherPeer.addIceCandidate(newIceCandidate)
//       .then(() => {
//         handleConnectionSuccess(peerConnection);
//       }).catch((error) => {
//         handleConnectionFailure(peerConnection, error);
//       });

//     trace(`${getPeerName(peerConnection)} ICE candidate:\n` +
//           `${event.candidate.candidate}.`);
//   }
// }

// // Logs that the connection succeeded.
// function handleConnectionSuccess(peerConnection) {
//   trace(`${getPeerName(peerConnection)} addIceCandidate success.`);
// };

// // Logs that the connection failed.
// function handleConnectionFailure(peerConnection, error) {
//   trace(`${getPeerName(peerConnection)} failed to add ICE Candidate:\n`+
//         `${error.toString()}.`);
// }

// // Logs changes to the connection state.
// function handleConnectionChange(event) {
//   const peerConnection = event.target;
//   console.log('ICE state change event: ', event);
//   trace(`${getPeerName(peerConnection)} ICE state: ` +
//         `${peerConnection.iceConnectionState}.`);
// }

// // Logs error when setting session description fails.
// function setSessionDescriptionError(error) {
//   trace(`Failed to create session description: ${error.toString()}.`);
// }

// // Logs success when setting session description.
// function setDescriptionSuccess(peerConnection, functionName) {
//   const peerName = getPeerName(peerConnection);
//   trace(`${peerName} ${functionName} complete.`);
// }

// // Logs success when localDescription is set.
// function setLocalDescriptionSuccess(peerConnection) {
//   setDescriptionSuccess(peerConnection, 'setLocalDescription');
// }

// // Logs success when remoteDescription is set.
// function setRemoteDescriptionSuccess(peerConnection) {
//   setDescriptionSuccess(peerConnection, 'setRemoteDescription');
// }

// // Logs offer creation and sets peer connection session descriptions.
// function createdOffer(description) {
//   trace(`Offer from localPeerConnection:\n${description.sdp}`);

//   trace('localPeerConnection setLocalDescription start.');
//   localPeerConnection.setLocalDescription(description)
//     .then(() => {
//       setLocalDescriptionSuccess(localPeerConnection);
//     }).catch(setSessionDescriptionError);

//   trace('remotePeerConnection setRemoteDescription start.');
//   remotePeerConnection.setRemoteDescription(description)
//     .then(() => {
//       setRemoteDescriptionSuccess(remotePeerConnection);
//     }).catch(setSessionDescriptionError);

//   trace('remotePeerConnection createAnswer start.');
//   remotePeerConnection.createAnswer()
//     .then(createdAnswer)
//     .catch(setSessionDescriptionError);
// }

// // Logs answer to offer creation and sets peer connection session descriptions.
// function createdAnswer(description) {
//   trace(`Answer from remotePeerConnection:\n${description.sdp}.`);

//   trace('remotePeerConnection setLocalDescription start.');
//   remotePeerConnection.setLocalDescription(description)
//     .then(() => {
//       setLocalDescriptionSuccess(remotePeerConnection);
//     }).catch(setSessionDescriptionError);

//   trace('localPeerConnection setRemoteDescription start.');
//   localPeerConnection.setRemoteDescription(description)
//     .then(() => {
//       setRemoteDescriptionSuccess(localPeerConnection);
//     }).catch(setSessionDescriptionError);
// }


// // Define and add behavior to buttons.

// // Define action buttons.
// const startButton = document.getElementById('startButton');
// const callButton = document.getElementById('callButton');
// const hangupButton = document.getElementById('hangupButton');

// // Set up initial action buttons status: disable call and hangup.
// callButton.disabled = true;
// hangupButton.disabled = true;


// // Handles start button action: creates local MediaStream.
// function startAction() {
//   startButton.disabled = true;
//   navigator.mediaDevices.getUserMedia(mediaStreamConstraints)
//     .then(gotLocalMediaStream).catch(handleLocalMediaStreamError);
//   trace('Requesting local stream.');
// }

// // Handles call button action: creates peer connection.
// function callAction() {
//   callButton.disabled = true;
//   hangupButton.disabled = false;

//   trace('Starting call.');
//   startTime = window.performance.now();

//   // Get local media stream tracks.
//   const videoTracks = localStream.getVideoTracks();
//   const audioTracks = localStream.getAudioTracks();
//   if (videoTracks.length > 0) {
//     trace(`Using video device: ${videoTracks[0].label}.`);
//   }
//   if (audioTracks.length > 0) {
//     trace(`Using audio device: ${audioTracks[0].label}.`);
//   }

//   const servers = null;  // Allows for RTC server configuration.

//   // Create peer connections and add behavior.
//   localPeerConnection = new RTCPeerConnection(servers);
//   trace('Created local peer connection object localPeerConnection.');

//   localPeerConnection.addEventListener('icecandidate', handleConnection);
//   localPeerConnection.addEventListener(
//     'iceconnectionstatechange', handleConnectionChange);

//   remotePeerConnection = new RTCPeerConnection(servers);
//   trace('Created remote peer connection object remotePeerConnection.');

//   remotePeerConnection.addEventListener('icecandidate', handleConnection);
//   remotePeerConnection.addEventListener(
//     'iceconnectionstatechange', handleConnectionChange);
//   remotePeerConnection.addEventListener('addstream', gotRemoteMediaStream);

//   // Add local stream to connection and create offer to connect.
//   localPeerConnection.addStream(localStream);
//   trace('Added local stream to localPeerConnection.');

//   trace('localPeerConnection createOffer start.');
//   localPeerConnection.createOffer(offerOptions)
//     .then(createdOffer).catch(setSessionDescriptionError);
// }

// // Handles hangup action: ends up call, closes connections and resets peers.
// function hangupAction() {
//   localPeerConnection.close();
//   remotePeerConnection.close();
//   localPeerConnection = null;
//   remotePeerConnection = null;
//   hangupButton.disabled = true;
//   callButton.disabled = false;
//   trace('Ending call.');
// }

// // Add click event handlers for buttons.
// startButton.addEventListener('click', startAction);
// callButton.addEventListener('click', callAction);
// hangupButton.addEventListener('click', hangupAction);


// // Define helper functions.

// // Gets the "other" peer connection.
// function getOtherPeer(peerConnection) {
//   return (peerConnection === localPeerConnection) ?
//       remotePeerConnection : localPeerConnection;
// }

// // Gets the name of a certain peer connection.
// function getPeerName(peerConnection) {
//   return (peerConnection === localPeerConnection) ?
//       'localPeerConnection' : 'remotePeerConnection';
// }

// // Logs an action (text) and the time when it happened on the console.
// function trace(text) {
//   text = text.trim();
//   const now = (window.performance.now() / 1000).toFixed(3);

//   console.log(now, text);
// }










// const mediaStreamConstraints = {
//     video: true,
//     audio: true,
// };
  
// // Define peer connections, streams and video elements.
// const localVideo = document.getElementById("localVideo");
// const remoteVideo = document.getElementById("remoteVideo");

// let localStream;
// let remoteStream;

// let localPeerConnection;
// let remotePeerConnection;


// // Define MediaStreams callbacks.
// // Sets the MediaStream as the video element src.
// function gotLocalMediaStream(mediaStream) {
//   localVideo.srcObject = mediaStream;
//   localStream = mediaStream;
//   trace('Received local stream.');
//   callButton.disabled = false;  // Enable call button.
// }

// // Handles error by logging a message to the console.
// function handleLocalMediaStreamError(error) {
//   trace(`navigator.getUserMedia error: ${error.toString()}.`);
// }

// // Handles remote MediaStream success by adding it as the remoteVideo src.
// function gotRemoteMediaStream(event) {
//   const mediaStream = event.stream;
//   remoteVideo.srcObject = mediaStream;
//   remoteStream = mediaStream;
//   trace('Remote peer connection received remote stream.');
// }


// // Add behavior for video streams.

// // Logs a message with the id and size of a video element.
// function logVideoLoaded(event) {
//   const video = event.target;
//   trace(`${video.id} videoWidth: ${video.videoWidth}px, ` +
//         `videoHeight: ${video.videoHeight}px.`);
// }

// // Logs a message with the id and size of a video element.
// // This event is fired when video begins streaming.
// function logResizedVideo(event) {
//   logVideoLoaded(event);

//   if (startTime) {
//     const elapsedTime = window.performance.now() - startTime;
//     startTime = null;
//     trace(`Setup time: ${elapsedTime.toFixed(3)}ms.`);
//   }
// }

// localVideo.addEventListener('loadedmetadata', logVideoLoaded);
// remoteVideo.addEventListener('loadedmetadata', logVideoLoaded);
// remoteVideo.addEventListener('onresize', logResizedVideo);

// // Define and add behavior to buttons.

// // Define action buttons.
// const startButton = document.getElementById('startButton');
// const callButton = document.getElementById('callButton');
// const hangupButton = document.getElementById('hangupButton');

// // Set up initial action buttons status: disable call and hangup.
// callButton.disabled = true;
// hangupButton.disabled = true;


// // Handles start button action: creates local MediaStream.
// function startAction() {
//   startButton.disabled = true;
//   navigator.mediaDevices.getUserMedia(mediaStreamConstraints)
//     .then(gotLocalMediaStream).catch(handleLocalMediaStreamError);
//   trace('Requesting local stream.');
// }

// // Handles call button action: creates peer connection.
// function callAction() {
//   callButton.disabled = true;
//   hangupButton.disabled = false;

//   trace('Starting call.');
//   startTime = window.performance.now();

//   // Get local media stream tracks.
//   const videoTracks = localStream.getVideoTracks();
//   const audioTracks = localStream.getAudioTracks();
//   if (videoTracks.length > 0) {
//     trace(`Using video device: ${videoTracks[0].label}.`);
//   }
//   if (audioTracks.length > 0) {
//     trace(`Using audio device: ${audioTracks[0].label}.`);
//   }

//   const servers = null;  // Allows for RTC server configuration.

//   // Create peer connections and add behavior.
//   localPeerConnection = new RTCPeerConnection(servers);
//   trace('Created local peer connection object localPeerConnection.');

//   localPeerConnection.addEventListener('icecandidate', handleConnection);
//   localPeerConnection.addEventListener(
//     'iceconnectionstatechange', handleConnectionChange);

//   remotePeerConnection = new RTCPeerConnection(servers);
//   trace('Created remote peer connection object remotePeerConnection.');

//   remotePeerConnection.addEventListener('icecandidate', handleConnection);
//   remotePeerConnection.addEventListener(
//     'iceconnectionstatechange', handleConnectionChange);
//   remotePeerConnection.addEventListener('addstream', gotRemoteMediaStream);

//   // Add local stream to connection and create offer to connect.
//   localPeerConnection.addStream(localStream);
//   trace('Added local stream to localPeerConnection.');

//   trace('localPeerConnection createOffer start.');
//   localPeerConnection.createOffer(offerOptions)
//     .then(createdOffer).catch(setSessionDescriptionError);
// }

// // Handles hangup action: ends up call, closes connections and resets peers.
// function hangupAction() {
//   localPeerConnection.close();
//   remotePeerConnection.close();
//   localPeerConnection = null;
//   remotePeerConnection = null;
//   hangupButton.disabled = true;
//   callButton.disabled = false;
//   trace('Ending call.');
// }

// // Add click event handlers for buttons.
// startButton.addEventListener('click', startAction);
// callButton.addEventListener('click', callAction);
// hangupButton.addEventListener('click', hangupAction);


// // Define helper functions.

// // Gets the "other" peer connection.
// function getOtherPeer(peerConnection) {
//   return (peerConnection === localPeerConnection) ?
//       remotePeerConnection : localPeerConnection;
// }

// // Gets the name of a certain peer connection.
// function getPeerName(peerConnection) {
//   return (peerConnection === localPeerConnection) ?
//       'localPeerConnection' : 'remotePeerConnection';
// }


// // Logs an action (text) and the time when it happened on the console.
// function trace(text) {
//     text = text.trim();
//     const now = (window.performance.now() / 1000).toFixed(3);
  
//     console.log(now, text);
//   }
  
//////////////////////////////////





const mediaConstraints = {
    video: true,
    audio: true,
};


$("#user_form").hide();
$("#connect_form").hide();
$("#send_form").hide();
$("#user_form").submit(set_username);
$("#connect_form").submit(initiate_rtc_connection);
// $("#send_form").submit(send_via_webrtc);
$("#set_name_btn").click(set_username);
$("#connect_btn").click(initiate_rtc_connection);
// $("#send_btn").click(send_via_webrtc);

var ws = null;
var username = "";
var distant_username = "";

// var myUsername = null;
// var targetUsername = null;      // To store username of other peer
// var rtc_connection = null;    // RTCPeerConnection

// To work both with and without addTrack() we need to note
// if it's available

var hasAddTrack = false;

// Output logging information to console.


//stun relay server in case of non working connection
var config = {
    "iceServers": [{
        "url": "stun:stun.l.google.com:19302"
    }]
};
var connection = {};

var rtc_connection;
var data_channel;



$("#ws-connect").click(function () {
    var roomName = 'st';
    var ws_prefix = 'ws://';
    if(window.location.protocol=="https:"){ ws_prefix = 'wss://'; }
    ws = new WebSocket(ws_prefix + window.location.host + '/ws/chat/' + roomName + '/');    

    ws.onopen = function (e) {
        console.log("Websocket opened");
        $("#user_form").show();
    }
    ws.onclose = function (e) {
        console.log("Websocket closed");
    }
    ws.onmessage = function (e) {
        console.log("onmessage: This function is run...");
        var json = JSON.parse(e.data);

        if (json.action == "candidate") {
            if (json.to == username) {
                handle_ice(json.data);
            }
        } else if (json.action == "offer") {
            if (json.to == username) {
                console.log("handle_offer!")
                distant_username = json.from;
                handle_offer(json.data)
            }
        } else if (json.action == "answer") {
            if (json.to == username) {
                handle_answer(json.data);
            }
        }

    }
    ws.onerror = function (e) {
        console.log("Websocket error");
    }
});

//Set username that will be used by the websocket server
//To know on which websocket connection it needs to send the datas
function set_username(e) {
    e.preventDefault();
    username = $("#user").val();
    var json = {
        action: 'login',
        data: username
    };
    ws.send(JSON.stringify(json));
    $("#connect_form").show();
    return false;
}

//Send text message directly via the channel
// function send_via_webrtc(e) {
//     console.log("send_via_webrtc: This function is run...");
//     e.preventDefault();
//     data_channel.send($("#message").val());
//     $('body').append('Me: <div class="message">' + $("#message").val() + '</div>');
//     $("#message").val('');
// }

//Method that send all the rtc negotation to the websocket server
function send_negotiation(type, sdp) {
    var json = {
        from: username,
        to: distant_username,
        action: type,
        data: sdp
    };
    ws.send(JSON.stringify(json));
}



//We iniate rtc connection by:
//- asking to create a channel...
//- creating an offer with the local sdp and send it to the peer
function initiate_rtc_connection(e) {
    var localStream = null;
    console.log("initiate_rtc_connection: This function is run...");
    e.preventDefault();
    distant_username = $("#distant_username").val();
    create_datachannel();



    navigator.mediaDevices.getUserMedia(mediaConstraints)
    .then(function(localStream) {
      console.log("-- Local video stream obtained");
      document.getElementById("localVideo").srcObject = localStream;
      if (hasAddTrack) {
        console.log("-- Adding tracks to the RTCPeerConnection");
        localStream.getTracks().forEach(track => rtc_connection.addTrack(track, localStream));
      } else {
        console.log("-- Adding stream to the RTCPeerConnection");
        rtc_connection.addStream(localStream);
      }
    })
    .catch(handleGetUserMediaError);


    // var sdpConstraints = {
    //     offerToReceiveAudio: false,
    //     offerToReceiveVideo: false
    // }
    var sdpConstraints = {
        offerToReceiveAudio: true,
        offerToReceiveVideo: true
    }
    rtc_connection.createOffer(sdpConstraints).then(function (sdp) {
        rtc_connection.setLocalDescription(sdp);
        send_negotiation("offer", sdp);
    }, function (err) {
        console.log(err)
    });
}


function create_datachannel() {
    console.log("Setting up a connection...");
  
    // Create an RTCPeerConnection which knows to use our chosen
    // STUN server.
  
    rtc_connection = new RTCPeerConnection(config, connection);
  
    // Do we have addTrack()? If not, we will use streams instead.
  
    hasAddTrack = (rtc_connection.addTrack !== undefined);
  
    // Set up event handlers for the ICE negotiation process.
    rtc_connection.onicecandidate = function (e) {
        if (!rtc_connection || !e || !e.candidate) return;
        var candidate = event.candidate;
        send_negotiation("candidate", candidate);
    }
    rtc_connection.onremovestream = handleRemoveStreamEvent;
    rtc_connection.oniceconnectionstatechange = handleICEConnectionStateChangeEvent;
    rtc_connection.onicegatheringstatechange = handleICEGatheringStateChangeEvent;
    rtc_connection.onsignalingstatechange = handleSignalingStateChangeEvent;
    // rtc_connection.onnegotiationneeded = handleNegotiationNeededEvent;
  
    // Because the deprecation of addStream() and the addstream event is recent,
    // we need to use those if addTrack() and track aren't available.
  
    if (hasAddTrack) {
        console.log("ontrack");
        rtc_connection.ontrack = handleTrackEvent;
    } else {
        console.log("onaddstream");
        rtc_connection.onaddstream = handleAddStreamEvent;
    }
    return rtc_connection;
}

// //We create a data channel from the RTCPeerConnection
// //On ice candidate we send it to the peer
// function create_datachannel() {
//     console.log("create_datachannel: This function is run...");
//     rtc_connection = new RTCPeerConnection(config, connection);
//     rtc_connection.onicecandidate = function (e) {
//         if (!rtc_connection || !e || !e.candidate) return;
//         var candidate = event.candidate;
//         send_negotiation("candidate", candidate);
//     }

//     data_channel = rtc_connection.createDataChannel("data_channel", {
//         reliable: false
//     });

//     data_channel.onopen = function () {
//         console.log("data channel opened")
//         $("#send_form").show();
//     };
//     data_channel.onclose = function () {
//         console.log("data channel closed")
//     };
//     data_channel.onerror = function () {
//         console.log("data channel error")
//     };

//     rtc_connection.ondatachannel = function (ev) {
//         ev.channel.onopen = function () {
//             console.log('Data channel is open and ready.');
//         };
//         ev.channel.onmessage = function (e) {
//             $('body').append(distant_username + ': <div class="message from">' + e.data + '</div>')
//         }
//     };

//     return rtc_connection
// }

//Create data channel + set remote description from peer offer
//then we create an answer and send it to the peer (via websocket server, thanks to the distant_username)
function handle_offer(offer) {
    console.log("handle_offer: This function is run...");
    var rtc_connection = create_datachannel();
    // rtc_connection.setRemoteDescription(new RTCSessionDescription(offer)).catch(e => {
    //     console.log("Error while setting remote description", e);
    // });
    rtc_connection.setRemoteDescription(new RTCSessionDescription(offer)).then(function () {
        console.log("Setting up the local media stream...");
        return navigator.mediaDevices.getUserMedia(mediaConstraints);
      })
      .then(function(stream) {
        console.log("-- Local video stream obtained");
        localStream = stream;
        document.getElementById("localVideo").srcObject = localStream;
    
        if (hasAddTrack) {
          console.log("-- Adding tracks to the RTCPeerConnection");
          localStream.getTracks().forEach(track =>
                rtc_connection.addTrack(track, localStream)
          );
        } else {
          console.log("-- Adding stream to the RTCPeerConnection");
          rtc_connection.addStream(localStream);
        }
      })
      .catch(handleGetUserMediaError);

    // var sdp_constraints = {
    //     'mandatory': {
    //         'OfferToReceiveAudio': false,
    //         'OfferToReceiveVideo': false
    //     }
    // };
    var sdp_constraints = {
        'mandatory': {
            'OfferToReceiveAudio': true,
            'OfferToReceiveVideo': true
        }
    };

    rtc_connection.createAnswer(sdp_constraints).then(function (sdp) {
        return rtc_connection.setLocalDescription(sdp).then(function () {
            send_negotiation("answer", sdp);
        })
    }, function (err) {
        console.log(err)
    });
};

//Handle peer answer (include configuration and media format)
function handle_answer(answer) {
    console.log("handle_answer: This function is run...");
    rtc_connection.setRemoteDescription(new RTCSessionDescription(answer));
};

//Handle ice data -> network configuratin (host candidate; server reflex candidate; relay candidate)
function handle_ice(iceCandidate) {
    console.log("handle_ice: This function is run...");
    rtc_connection.addIceCandidate(new RTCIceCandidate(iceCandidate)).catch(e => {
        console.log("Error while adding candidate ", e);
    })
}

























// Called by the WebRTC layer to let us know when it's time to
// begin (or restart) ICE negotiation. Starts by creating a WebRTC
// offer, then sets it as the description of our local media
// (which configures our local media stream), then sends the
// description to the callee as an offer. This is a proposed media
// format, codec, resolution, etc.

function handleNegotiationNeededEvent() {
  console.log("*** Negotiation needed");
  console.log("---> Creating offer");
  rtc_connection.createOffer().then(function(offer) {
    console.log("---> Creating new description object to send to remote peer");
    return rtc_connection.setLocalDescription(offer);
  }).then(function() {
      console.log("---> Sending offer to remote peer");
      send_negotiation("offer", rtc_connection.localDescription);
    });
}

    //     sendToServer({
//       name: myUsername,
//       target: targetUsername,
//       type: "video-offer",
//       sdp: rtc_connection.localDescription
//     });
//   })
//   .catch(reportError);

// Called by the WebRTC layer when events occur on the media tracks
// on our WebRTC call. This includes when streams are added to and
// removed from the call.
//
// track events include the following fields:
//
// RTCRtpReceiver       receiver
// MediaStreamTrack     track
// MediaStream[]        streams
// RTCRtpTransceiver    transceiver

function handleTrackEvent(event) {
  console.log("*** Track event");
  document.getElementById("remoteVideo").srcObject = event.streams[0];
  document.getElementById("hangupButton").disabled = false;
}

// Called by the WebRTC layer when a stream starts arriving from the
// remote peer. We use this to update our user interface, in this
// example.

function handleAddStreamEvent(event) {
  console.log("*** Stream added");
  document.getElementById("remoteVideo").srcObject = event.stream;
  document.getElementById("hangupButton").disabled = false;
}

// An event handler which is called when the remote end of the connection
// removes its stream. We consider this the same as hanging up the call.
// It could just as well be treated as a "mute".
//
// Note that currently, the spec is hazy on exactly when this and other
// "connection failure" scenarios should occur, so sometimes they simply
// don't happen.

function handleRemoveStreamEvent(event) {
  console.log("*** Stream removed");
  closeVideoCall();
}

// Handles |icecandidate| events by forwarding the specified
// ICE candidate (created by our local ICE agent) to the other
// peer through the signaling server.


// Handle |iceconnectionstatechange| events. This will detect
// when the ICE connection is closed, failed, or disconnected.
//
// This is called when the state of the ICE agent changes.

function handleICEConnectionStateChangeEvent(event) {
  console.log("*** ICE connection state changed to " + rtc_connection.iceConnectionState);

  switch(rtc_connection.iceConnectionState) {
    case "closed":
    case "failed":
    case "disconnected":
      closeVideoCall();
      break;
  }
}

// Set up a |signalingstatechange| event handler. This will detect when
// the signaling connection is closed.
//
// NOTE: This will actually move to the new RTCPeerConnectionState enum
// returned in the property RTCPeerConnection.connectionState when
// browsers catch up with the latest version of the specification!

function handleSignalingStateChangeEvent(event) {
  console.log("*** WebRTC signaling state changed to: " + rtc_connection.signalingState);
  switch(rtc_connection.signalingState) {
    case "closed":
      closeVideoCall();
      break;
  }
}

// Handle the |icegatheringstatechange| event. This lets us know what the
// ICE engine is currently working on: "new" means no networking has happened
// yet, "gathering" means the ICE engine is currently gathering candidates,
// and "complete" means gathering is complete. Note that the engine can
// alternate between "gathering" and "complete" repeatedly as needs and
// circumstances change.
//
// We don't need to do anything when this happens, but we log it to the
// console so you can see what's going on when playing with the sample.

function handleICEGatheringStateChangeEvent(event) {
  console.log("*** ICE gathering state changed to: " + rtc_connection.iceGatheringState);
}

// Given a message containing a list of usernames, this function
// populates the user list box with those names, making each item
// clickable to allow starting a video call.

// function handleUserlistMsg(msg) {
//   var i;

//   var listElem = document.getElementById("userlistbox");

//   // Remove all current list members. We could do this smarter,
//   // by adding and updating users instead of rebuilding from
//   // scratch but this will do for this sample.

//   while (listElem.firstChild) {
//     listElem.removeChild(listElem.firstChild);
//   }

//   // Add member names from the received list

//   for (i=0; i < msg.users.length; i++) {
//     var item = document.createElement("li");
//     item.appendChild(document.createTextNode(msg.users[i]));
//     item.addEventListener("click", invite, false);

//     listElem.appendChild(item);
//   }
// }

// Close the RTCPeerConnection and reset variables so that the user can
// make or receive another call if they wish. This is called both
// when the user hangs up, the other user hangs up, or if a connection
// failure is detected.

function closeVideoCall() {
  var remoteVideo = document.getElementById("remoteVideo");
  var localVideo = document.getElementById("localVideo");

  console.log("Closing the call");

  // Close the RTCPeerConnection

  if (rtc_connection) {
    console.log("--> Closing the peer connection");

    // Disconnect all our event listeners; we don't want stray events
    // to interfere with the hangup while it's ongoing.

    rtc_connection.onaddstream = null;  // For older implementations
    rtc_connection.ontrack = null;      // For newer ones
    rtc_connection.onremovestream = null;
    rtc_connection.onnicecandidate = null;
    rtc_connection.oniceconnectionstatechange = null;
    rtc_connection.onsignalingstatechange = null;
    rtc_connection.onicegatheringstatechange = null;
    rtc_connection.onnotificationneeded = null;

    // Stop the videos

    if (remoteVideo.srcObject) {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    if (localVideo.srcObject) {
      localVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    remoteVideo.src = null;
    localVideo.src = null;

    // Close the peer connection

    rtc_connection.close();
    rtc_connection = null;
  }

  // Disable the hangup button

  document.getElementById("hangupButton").disabled = true;

  targetUsername = null;
}

// Handle the "hang-up" message, which is sent if the other peer
// has hung up the call or otherwise disconnected.

function handleHangUpMsg(msg) {
  console.log("*** Received hang up notification from other peer");

  closeVideoCall();
}

// Hang up the call by closing our end of the connection, then
// sending a "hang-up" message to the other peer (keep in mind that
// the signaling is done on a different connection). This notifies
// the other peer that the connection should be terminated and the UI
// returned to the "no call in progress" state.

function hangUpCall() {
  closeVideoCall();
//   sendToServer({
//     name: myUsername,
//     target: targetUsername,
//     type: "hang-up"
//   });
}

// Handle a click on an item in the user list by inviting the clicked
// user to video chat. Note that we don't actually send a message to
// the callee here -- calling RTCPeerConnection.addStream() issues
// a |notificationneeded| event, so we'll let our handler for that
// make the offer.

// function invite(evt) {
//   log("Starting to prepare an invitation");
//   if (rtc_connection) {
//     alert("You can't start a call because you already have one open!");
//   } else {
//     var clickedUsername = evt.target.textContent;

//     // Don't allow users to call themselves, because weird.

//     if (clickedUsername === myUsername) {
//       alert("I'm afraid I can't let you talk to yourself. That would be weird.");
//       return;
//     }

//     // Record the username being called for future reference

//     targetUsername = clickedUsername;
//     log("Inviting user " + targetUsername);

//     // Call createPeerConnection() to create the RTCPeerConnection.

//     log("Setting up connection to invite user: " + targetUsername);
//     createPeerConnection();

//     // Now configure and create the local stream, attach it to the
//     // "preview" box (id "local_video"), and add it to the
//     // RTCPeerConnection.

//     log("Requesting webcam access...");

//     navigator.mediaDevices.getUserMedia(mediaConstraints)
//     .then(function(localStream) {
//       log("-- Local video stream obtained");
//       document.getElementById("localVideo").srcObject = localStream;

//       if (hasAddTrack) {
//         log("-- Adding tracks to the RTCPeerConnection");
//         localStream.getTracks().forEach(track => rtc_connection.addTrack(track, localStream));
//       } else {
//         log("-- Adding stream to the RTCPeerConnection");
//         rtc_connection.addStream(localStream);
//       }
//     })
//     .catch(handleGetUserMediaError);
//   }
// }

// Accept an offer to video chat. We configure our local settings,
// create our RTCPeerConnection, get and attach our local camera
// stream, then create and send an answer to the caller.

// function handleVideoOfferMsg(msg) {
//   var localStream = null;

//   targetUsername = msg.name;

//   // Call createPeerConnection() to create the RTCPeerConnection.

//   log("Starting to accept invitation from " + targetUsername);
//   createPeerConnection();

//   // We need to set the remote description to the received SDP offer
//   // so that our local WebRTC layer knows how to talk to the caller.

//   var desc = new RTCSessionDescription(msg.sdp);

//   rtc_connection.setRemoteDescription(desc).then(function () {
//     log("Setting up the local media stream...");
//     return navigator.mediaDevices.getUserMedia(mediaConstraints);
//   })
//   .then(function(stream) {
//     log("-- Local video stream obtained");
//     localStream = stream;
//     document.getElementById("local_video").srcObject = localStream;

//     if (hasAddTrack) {
//       log("-- Adding tracks to the RTCPeerConnection");
//       localStream.getTracks().forEach(track =>
//             rtc_connection.addTrack(track, localStream)
//       );
//     } else {
//       log("-- Adding stream to the RTCPeerConnection");
//       rtc_connection.addStream(localStream);
//     }
//   })
//   .then(function() {
//     log("------> Creating answer");
//     // Now that we've successfully set the remote description, we need to
//     // start our stream up locally then create an SDP answer. This SDP
//     // data describes the local end of our call, including the codec
//     // information, options agreed upon, and so forth.
//     return rtc_connection.createAnswer();
//   })
//   .then(function(answer) {
//     log("------> Setting local description after creating answer");
//     // We now have our answer, so establish that as the local description.
//     // This actually configures our end of the call to match the settings
//     // specified in the SDP.
//     return rtc_connection.setLocalDescription(answer);
//   })
//   .then(function() {
//     var msg = {
//       name: myUsername,
//       target: targetUsername,
//       type: "video-answer",
//       sdp: rtc_connection.localDescription
//     };

//     // We've configured our end of the call now. Time to send our
//     // answer back to the caller so they know that we want to talk
//     // and how to talk to us.

//     log("Sending answer packet back to other peer");
//     // sendToServer(msg);
//   })
//   .catch(handleGetUserMediaError);
// }

// Responds to the "video-answer" message sent to the caller
// once the callee has decided to accept our request to talk.

// function handleVideoAnswerMsg(msg) {
//   console.log("Call recipient has accepted our call");

//   // Configure the remote description, which is the SDP payload
//   // in our "video-answer" message.
//   var desc = new RTCSessionDescription(msg.sdp);
//   rtc_connection.setRemoteDescription(desc).catch(reportError);
// }

// A new ICE candidate has been received from the other peer. Call
// RTCPeerConnection.addIceCandidate() to send it along to the
// local ICE framework.

// function handleNewICECandidateMsg(msg) {
//   var candidate = new RTCIceCandidate(msg.candidate);

//   console.log("Adding received ICE candidate: " + JSON.stringify(candidate));
//   rtc_connection.addIceCandidate(candidate)
//     .catch(reportError);
// }

// Handle errors which occur when trying to access the local media
// hardware; that is, exceptions thrown by getUserMedia(). The two most
// likely scenarios are that the user has no camera and/or microphone
// or that they declined to share their equipment when prompted. If
// they simply opted not to share their media, that's not really an
// error, so we won't present a message in that situation.

function handleGetUserMediaError(e) {
  console.log(e);
  switch(e.name) {
    case "NotFoundError":
      alert("Unable to open your call because no camera and/or microphone" +
            "were found.");
      break;
    case "SecurityError":
    case "PermissionDeniedError":
      // Do nothing; this is the same as the user canceling the call.
      break;
    default:
      alert("Error opening your camera and/or microphone: " + e.message);
      break;
  }

  // Make sure we shut down our end of the RTCPeerConnection so we're
  // ready to try again.

  closeVideoCall();
}

// Handles reporting errors. Currently, we just dump stuff to console but
// in a real-world application, an appropriate (and user-friendly)
// error message should be displayed.

// function reportError(errMessage) {
//   log_error("Error " + errMessage.name + ": " + errMessage.message);
// }