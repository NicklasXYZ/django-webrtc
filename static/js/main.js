'use strict';

// Set up media stream constant and parameters.

// In this codelab, you will be streaming video only: "video: true".
// Audio will not be streamed because it is set to "audio: false" by default.
const mediaStreamConstraints = {
  video: true,
};

// Set up to exchange only video.
const offerOptions = {
  offerToReceiveVideo: 1,
};

// Define initial start time of the call (defined as connection between peers).
let startTime = null;

// Define peer connections, streams and video elements.
const localVideo = document.getElementById('localVideo');
const remoteVideo = document.getElementById('remoteVideo');

let localStream;
let remoteStream;

let localPeerConnection;
let remotePeerConnection;


// Define MediaStreams callbacks.

// Sets the MediaStream as the video element src.
function gotLocalMediaStream(mediaStream) {
  localVideo.srcObject = mediaStream;
  localStream = mediaStream;
  trace('Received local stream.');
  callButton.disabled = false;  // Enable call button.
}

// Handles error by logging a message to the console.
function handleLocalMediaStreamError(error) {
  trace(`navigator.getUserMedia error: ${error.toString()}.`);
}

// Handles remote MediaStream success by adding it as the remoteVideo src.
function gotRemoteMediaStream(event) {
  const mediaStream = event.stream;
  remoteVideo.srcObject = mediaStream;
  remoteStream = mediaStream;
  trace('Remote peer connection received remote stream.');
}


// Add behavior for video streams.

// Logs a message with the id and size of a video element.
function logVideoLoaded(event) {
  const video = event.target;
  trace(`${video.id} videoWidth: ${video.videoWidth}px, ` +
        `videoHeight: ${video.videoHeight}px.`);
}

// Logs a message with the id and size of a video element.
// This event is fired when video begins streaming.
function logResizedVideo(event) {
  logVideoLoaded(event);

  if (startTime) {
    const elapsedTime = window.performance.now() - startTime;
    startTime = null;
    trace(`Setup time: ${elapsedTime.toFixed(3)}ms.`);
  }
}

localVideo.addEventListener('loadedmetadata', logVideoLoaded);
remoteVideo.addEventListener('loadedmetadata', logVideoLoaded);
remoteVideo.addEventListener('onresize', logResizedVideo);


// Define RTC peer connection behavior.

// Connects with new peer candidate.
function handleConnection(event) {
  const peerConnection = event.target;
  const iceCandidate = event.candidate;
  console.log("iceCandidate:" + iceCandidate);
  console.log("peerConnection: " + peerConnection);

  if (iceCandidate) {
    const newIceCandidate = new RTCIceCandidate(iceCandidate);
    const otherPeer = getOtherPeer(peerConnection);

    otherPeer.addIceCandidate(newIceCandidate)
      .then(() => {
        handleConnectionSuccess(peerConnection);
      }).catch((error) => {
        handleConnectionFailure(peerConnection, error);
      });

    trace(`${getPeerName(peerConnection)} ICE candidate:\n` +
          `${event.candidate.candidate}.`);
  }
}

// Logs that the connection succeeded.
function handleConnectionSuccess(peerConnection) {
  trace(`${getPeerName(peerConnection)} addIceCandidate success.`);
};

// Logs that the connection failed.
function handleConnectionFailure(peerConnection, error) {
  trace(`${getPeerName(peerConnection)} failed to add ICE Candidate:\n`+
        `${error.toString()}.`);
}

// Logs changes to the connection state.
function handleConnectionChange(event) {
  const peerConnection = event.target;
  console.log('ICE state change event: ', event);
  trace(`${getPeerName(peerConnection)} ICE state: ` +
        `${peerConnection.iceConnectionState}.`);
}

// Logs error when setting session description fails.
function setSessionDescriptionError(error) {
  trace(`Failed to create session description: ${error.toString()}.`);
}

// Logs success when setting session description.
function setDescriptionSuccess(peerConnection, functionName) {
  const peerName = getPeerName(peerConnection);
  trace(`${peerName} ${functionName} complete.`);
}

// Logs success when localDescription is set.
function setLocalDescriptionSuccess(peerConnection) {
  setDescriptionSuccess(peerConnection, 'setLocalDescription');
}

// Logs success when remoteDescription is set.
function setRemoteDescriptionSuccess(peerConnection) {
  setDescriptionSuccess(peerConnection, 'setRemoteDescription');
}

// Logs offer creation and sets peer connection session descriptions.
function createdOffer(description) {
  trace(`Offer from localPeerConnection:\n${description.sdp}`);

  trace('localPeerConnection setLocalDescription start.');
  localPeerConnection.setLocalDescription(description)
    .then(() => {
      setLocalDescriptionSuccess(localPeerConnection);
    }).catch(setSessionDescriptionError);

  trace('remotePeerConnection setRemoteDescription start.');
  remotePeerConnection.setRemoteDescription(description)
    .then(() => {
      setRemoteDescriptionSuccess(remotePeerConnection);
    }).catch(setSessionDescriptionError);

  trace('remotePeerConnection createAnswer start.');
  remotePeerConnection.createAnswer()
    .then(createdAnswer)
    .catch(setSessionDescriptionError);
}

// Logs answer to offer creation and sets peer connection session descriptions.
function createdAnswer(description) {
  trace(`Answer from remotePeerConnection:\n${description.sdp}.`);

  trace('remotePeerConnection setLocalDescription start.');
  remotePeerConnection.setLocalDescription(description)
    .then(() => {
      setLocalDescriptionSuccess(remotePeerConnection);
    }).catch(setSessionDescriptionError);

  trace('localPeerConnection setRemoteDescription start.');
  localPeerConnection.setRemoteDescription(description)
    .then(() => {
      setRemoteDescriptionSuccess(localPeerConnection);
    }).catch(setSessionDescriptionError);
}


// Define and add behavior to buttons.

// Define action buttons.
const startButton = document.getElementById('startButton');
const callButton = document.getElementById('callButton');
const hangupButton = document.getElementById('hangupButton');

// Set up initial action buttons status: disable call and hangup.
callButton.disabled = true;
hangupButton.disabled = true;


// Handles start button action: creates local MediaStream.
function startAction() {
  startButton.disabled = true;
  navigator.mediaDevices.getUserMedia(mediaStreamConstraints)
    .then(gotLocalMediaStream).catch(handleLocalMediaStreamError);
  trace('Requesting local stream.');
}

// Handles call button action: creates peer connection.
function callAction() {
  callButton.disabled = true;
  hangupButton.disabled = false;

  trace('Starting call.');
  startTime = window.performance.now();

  // Get local media stream tracks.
  const videoTracks = localStream.getVideoTracks();
  const audioTracks = localStream.getAudioTracks();
  if (videoTracks.length > 0) {
    trace(`Using video device: ${videoTracks[0].label}.`);
  }
  if (audioTracks.length > 0) {
    trace(`Using audio device: ${audioTracks[0].label}.`);
  }

  const servers = null;  // Allows for RTC server configuration.

  // Create peer connections and add behavior.
  localPeerConnection = new RTCPeerConnection(servers);
  trace('Created local peer connection object localPeerConnection.');

  localPeerConnection.addEventListener('icecandidate', handleConnection);
  localPeerConnection.addEventListener(
    'iceconnectionstatechange', handleConnectionChange);

  remotePeerConnection = new RTCPeerConnection(servers);
  trace('Created remote peer connection object remotePeerConnection.');

  remotePeerConnection.addEventListener('icecandidate', handleConnection);
  remotePeerConnection.addEventListener(
    'iceconnectionstatechange', handleConnectionChange);
  remotePeerConnection.addEventListener('addstream', gotRemoteMediaStream);

  // Add local stream to connection and create offer to connect.
  localPeerConnection.addStream(localStream);
  trace('Added local stream to localPeerConnection.');

  trace('localPeerConnection createOffer start.');
  localPeerConnection.createOffer(offerOptions)
    .then(createdOffer).catch(setSessionDescriptionError);
}

// Handles hangup action: ends up call, closes connections and resets peers.
function hangupAction() {
  localPeerConnection.close();
  remotePeerConnection.close();
  localPeerConnection = null;
  remotePeerConnection = null;
  hangupButton.disabled = true;
  callButton.disabled = false;
  trace('Ending call.');
}

// Add click event handlers for buttons.
startButton.addEventListener('click', startAction);
callButton.addEventListener('click', callAction);
hangupButton.addEventListener('click', hangupAction);


// Define helper functions.

// Gets the "other" peer connection.
function getOtherPeer(peerConnection) {
  return (peerConnection === localPeerConnection) ?
      remotePeerConnection : localPeerConnection;
}

// Gets the name of a certain peer connection.
function getPeerName(peerConnection) {
  return (peerConnection === localPeerConnection) ?
      'localPeerConnection' : 'remotePeerConnection';
}

// Logs an action (text) and the time when it happened on the console.
function trace(text) {
  text = text.trim();
  const now = (window.performance.now() / 1000).toFixed(3);

  console.log(now, text);
}





// $("#user_form").hide();
// $("#connect_form").hide();
// $("#send_form").hide();
// $("#user_form").submit(set_username);
// $("#connect_form").submit(initiate_rtc_connection);
// $("#send_form").submit(send_via_webrtc);
// $("#set_name_btn").click(set_username);
// $("#connect_btn").click(initiate_rtc_connection);
// $("#send_btn").click(send_via_webrtc);

// var ws = null;
// var username = "";
// var distant_username = "";

// //stun relay server in case of non working connection
// var config = {
//     "iceServers": [{
//         "url": "stun:stun.l.google.com:19302"
//     }]
// };
// var connection = {};

// var rtc_connection;
// var data_channel;



// $("#ws-connect").click(function () {
//     ws = new WebSocket("ws://0.0.0.0:5000");

//     ws.onopen = function (e) {
//         console.log("Websocket opened");
//         $("#user_form").show();
//     }
//     ws.onclose = function (e) {
//         console.log("Websocket closed");
//     }
//     ws.onmessage = function (e) {
//         var json = JSON.parse(e.data);

//         if (json.action == "candidate") {
//             if (json.to == username) {
//                 handle_ice(json.data);
//             }
//         } else if (json.action == "offer") {
//             if (json.to == username) {
//                 distant_username = json.from;
//                 handle_offer(json.data)
//             }
//         } else if (json.action == "answer") {
//             if (json.to == username) {
//                 handle_answer(json.data);
//             }
//         }

//     }
//     ws.onerror = function (e) {
//         console.log("Websocket error");
//     }
// });

// //Set username that will be used by the websocket server
// //To know on which websocket connection it needs to send the datas
// function set_username(e) {
//     e.preventDefault();
//     username = $("#user").val();
//     var json = {
//         action: 'login',
//         data: username
//     };
//     ws.send(JSON.stringify(json));
//     $("#connect_form").show();
//     return false;
// }

// //Send text message directly via the channel
// function send_via_webrtc(e) {
//     e.preventDefault();
//     data_channel.send($("#message").val());
//     $('body').append('Me: <div class="message">' + $("#message").val() + '</div>');
//     $("#message").val('');
// }

// //Method that send all the rtc negotation to the websocket server
// function send_negotiation(type, sdp) {
//     var json = {
//         from: username,
//         to: distant_username,
//         action: type,
//         data: sdp
//     };
//     ws.send(JSON.stringify(json));
// }

// //We iniate rtc connection by:
// //- asking to create a channel...
// //- creating an offer with the local sdp and send it to the peer
// function initiate_rtc_connection(e) {
//     e.preventDefault();
//     distant_username = $("#distant_username").val();
//     create_datachannel();

//     var sdpConstraints = {
//         offerToReceiveAudio: false,
//         offerToReceiveVideo: false
//     }
//     rtc_connection.createOffer(sdpConstraints).then(function (sdp) {
//         rtc_connection.setLocalDescription(sdp);
//         send_negotiation("offer", sdp);
//     }, function (err) {
//         console.log(err)
//     });
// }

// //We create a data channel from the RTCPeerConnection
// //On ice candidate we send it to the peer
// function create_datachannel() {
//     rtc_connection = new RTCPeerConnection(config, connection);
//     rtc_connection.onicecandidate = function (e) {
//         if (!rtc_connection || !e || !e.candidate) return;
//         var candidate = event.candidate;
//         send_negotiation("candidate", candidate);
//     }

//     data_channel = rtc_connection.createDataChannel("data_channel", {
//         reliable: false
//     });

//     data_channel.onopen = function () {
//         console.log("data channel opened")
//         $("#send_form").show();
//     };
//     data_channel.onclose = function () {
//         console.log("data channel closed")
//     };
//     data_channel.onerror = function () {
//         console.log("data channel error")
//     };

//     rtc_connection.ondatachannel = function (ev) {
//         ev.channel.onopen = function () {
//             console.log('Data channel is open and ready.');
//         };
//         ev.channel.onmessage = function (e) {
//             $('body').append(distant_username + ': <div class="message from">' + e.data + '</div>')
//         }
//     };

//     return rtc_connection
// }

// //Create data channel + set remote description from peer offer
// //then we create an answer and send it to the peer (via websocket server, thanks to the distant_username)
// function handle_offer(offer) {
//     var rtc_connection = create_datachannel();
//     rtc_connection.setRemoteDescription(new RTCSessionDescription(offer)).catch(e => {
//         console.log("Error while setting remote description", e);
//     });

//     var sdp_constraints = {
//         'mandatory': {
//             'OfferToReceiveAudio': false,
//             'OfferToReceiveVideo': false
//         }
//     };

//     rtc_connection.createAnswer(sdp_constraints).then(function (sdp) {
//         return rtc_connection.setLocalDescription(sdp).then(function () {
//             send_negotiation("answer", sdp);
//         })
//     }, function (err) {
//         console.log(err)
//     });
// };

// //Handle peer answer (include configuration and media format)
// function handle_answer(answer) {
//     rtc_connection.setRemoteDescription(new RTCSessionDescription(answer));
// };

// //Handle ice data -> network configuratin (host candidate; server reflex candidate; relay candidate)
// function handle_ice(iceCandidate) {
//     rtc_connection.addIceCandidate(new RTCIceCandidate(iceCandidate)).catch(e => {
//         console.log("Error while adding candidate ", e);
//     })
// }